#include "Logging.h"

std::string LogLevelToString(Logger::Level level)
{
	switch (level)
	{
	case Logger::Level::Info:
		return "Info";
	case Logger::Level::Warning:
		return "Warning";
	case Logger::Level::Error:
		return "Error";
	default:
		return "";
	}
}

Logger::Logger(std::ostream & os) :
	os{ os }

{
	// Empty
}



void Logger::log(std::string  message)
{
	os << message << std::endl;
}

std::string Logger::showMessage(Logger::Message message, Logger::Level level)
{
	std::string myString = "";
	switch (message)
	{
	case Logger::Message::Start:
	{
		myString = "[" + LogLevelToString(Logger::Level::Info) + "]" + " Game has started";
		return myString;
		break;
	}
	case Logger::Message::PlayerChanged:
	{
		myString = "[" + LogLevelToString(Logger::Level::Info) + "]" + " Player has changed";
		return myString;
		break;
	}
	case Logger::Message::PlayerPlaced:
	{
		myString = "[" + LogLevelToString(Logger::Level::Info) + "]" + " Current player has placed a piece on board";
		return myString;
		break;
	}
	case Logger::Message::PlayerRefilled:
	{
		myString = "[" + LogLevelToString(Logger::Level::Info) + "]" + " Current player has refilled the piece support";
		return myString;
		break;
	}

	}
}
