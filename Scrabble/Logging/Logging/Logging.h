#pragma once
#include <iostream>
#include <string>

class __declspec(dllexport) Logger
{
public:
	enum class Level
	{
		Info,
		Warning,
		Error
	};
	enum class Message
	{
		Start,
		PlayerChanged,
		PlayerPlaced,
		PlayerRefilled
	};

public:
	Logger(std::ostream& os);


	void log(std::string message);
	std::string showMessage(Logger::Message message, Logger::Level level);


private:
	std::ostream& os;

};
