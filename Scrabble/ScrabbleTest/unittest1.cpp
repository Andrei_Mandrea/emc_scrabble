#include "stdafx.h"
#include "CppUnitTest.h"
#include "Bag.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ScrabbleTest
{		
	TEST_CLASS(BagTest)
	{
	public:
		
		TEST_METHOD(ConstructorTest)
		{
			Bag b;
			Assert::IsTrue(b.getNoOfPieces() == 100);
		}

		TEST_METHOD(getPieceTest)
		{
			Bag b;
			b.getPiece();
			Assert::IsTrue(b.getNoOfPieces() == 99);
			
		}
		
	};
}