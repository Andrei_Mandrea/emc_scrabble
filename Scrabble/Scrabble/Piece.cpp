#include "Piece.h"


Piece::Piece(const char& m_character, const int& m_point) :
	character(m_character),
	point(m_point)
{
}

char Piece::getCharacter() const
{
	return character;
}

int Piece::getPoints() const
{
	return point;
}

void Piece::setPieceAsDefault()
{
	this->character = '!';
	this->point = -1;
}
bool Piece::compare(const Piece& piece2) const
{
	if (this->character == piece2.getCharacter())
		return true;
	return false;
}
std::ostream & operator<<(std::ostream & os, const Piece & p)
{
	os << p.character << " " << p.point;
	return os;
}


