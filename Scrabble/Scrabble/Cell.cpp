#include "Cell.h"


Cell::Cell(const int& m_x,const int& m_y) :
	x(m_x),
	y(m_y),
	bonus(Simple)
{

}
Piece Cell::getPiece() const
{
	return piece;
}

void Cell::setPiece(const Piece& p)
{
	piece = p;
}

Cell::Bonus Cell::getBonus() const
{
	return bonus;
}

void Cell::setBonus(const Bonus& b)
{
	bonus = b;
}

int Cell::getColor() const
{
	return color;
}

void Cell::setColor(const int& color)
{
	this->color = color;
}

int Cell::getScoreByBonus()
{
	int scoreOfCell = piece.getPoints();
	if (bonus == 0)
		scoreOfCell = 2 * piece.getPoints();
	if(bonus==1)
		scoreOfCell = 3 * piece.getPoints();
	if(bonus==4)
		scoreOfCell = 1 * piece.getPoints();
	return scoreOfCell;
}
