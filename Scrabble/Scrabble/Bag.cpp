#include "Bag.h"
#include "Piece.h"
#include <time.h>

Bag::Bag() :
	noOfPieces(initialNoOfPieces)
{
	
	pieces.resize(initialNoOfPieces);
	srand(time(nullptr));
	const std::vector<char> characters = { 'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',' ' };
	const std::vector<int> points = { 1,3,3,2,1,4,2,4,1,8,5,1,3,1,1,3,10,1,1,1,1,4,4,8,4,10,0 };
	const std::vector<int> repeats = { 9,2,2,4,12,2,3,2,9,1,1,4,2,6,8,2,1,6,4,6,4,2,2,1,2,1,2 };
	int n = 0;
	for (int i = 0; i < characters.size(); i++)
	{
		for (int j = 0; j < repeats[i]; j++, n++)

		{
			pieces[n] = Piece(characters[i], points[i]);
		}
	}
}

int Bag::getNoOfPieces() const
{
	return noOfPieces;
}

Piece Bag::getPiece() 
{
	if (noOfPieces != 0)
	{
		int random = rand() % noOfPieces;
		Piece P = pieces[random];
		pieces[random] = pieces[noOfPieces - 1];
		pieces[noOfPieces - 1] = P;
		noOfPieces--;
		return P;
	}
}

void Bag::addOnePiece(const Piece & piece)
{
	pieces.push_back(piece);
	noOfPieces++;
}
void Bag::removeOnePiece(Piece & piece)
{
	int poz = 999;
	for (int index = 0; index < noOfPieces; index++)
	{
		if (pieces[index].compare(piece) == true)
		{
			poz = index;
			break;
		}
	}
	pieces.erase(pieces.begin() + poz);
	noOfPieces--;

}
void Bag::printVectorOfPieces(std::ostream& os) const
{
	for (Piece p : pieces)
	{
		os << " ";
		os<<p;
		os << " ";
	}
	os << std::endl;
	os << pieces.size();
}
