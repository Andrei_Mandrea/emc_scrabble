#pragma once
#include <iostream>
#include <string>
#include "Bag.h"
#include "PieceSupport.h"
#include "Board.h"

class Player
{
private:
	Bag bag;
	PieceSupport pieceSupport;

public:
	Player()=default;
	Player(const std::string& name,Bag& bag);

	std::string getPlayerName() const;
	int getPlayerScore() const;
	void setPlayerScore(const int& score);
	PieceSupport getPieceSupport() const;

	//Methods from Piece
	void changePieces(std::istream& in);
	void refillSupport();
	void mixPieces();
	
	void checkTheWord(std::istream& in,Board& board);
	void putInCell(std::istream& in,Board& board);
	void printPlayer() const;
	friend std::ostream& operator << (std::ostream& os, const Player& player);
private:
	std::string playerName;
	int playerScore;


};