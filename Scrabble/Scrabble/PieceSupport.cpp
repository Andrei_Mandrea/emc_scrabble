#include "PieceSupport.h"
#include <random>

static const int kIndexOffset = 1;

PieceSupport::PieceSupport()
{
	//default constructor
	PiecesOnSupport.resize(noPiecesOnSupport);
	for (int index = 0; index < noPiecesOnSupport; index++)
	{
		PiecesOnSupport[index].setPieceAsDefault();
	}
}

PieceSupport::PieceSupport(Bag& m_bag) :
	bag(m_bag)
{
	//std::cout<<"PieceSUPPORT CONSTRUCTED\n";
	PiecesOnSupport.resize(noPiecesOnSupport);
	for (int index = 0; index < noPiecesOnSupport; index++)
	{
		PiecesOnSupport[index] = m_bag.getPiece();
	}

}

void PieceSupport::mixPieces()
{
	
	static std::mt19937 randomNumberGenerator(std::random_device{}());
	std::shuffle(PiecesOnSupport.begin(), PiecesOnSupport.end(),randomNumberGenerator);
}


void PieceSupport::changePieces(int pieceToDrop)
{
	PiecesOnSupport[pieceToDrop - kIndexOffset].setPieceAsDefault();
}

void PieceSupport::refillSupport()
{
	for (int index = 0; index < noPiecesOnSupport; index++)
	{
		if (PiecesOnSupport[index].getCharacter() == '!')
			PiecesOnSupport[index] = bag.getPiece();
	}
	//printSupport();
}


/*void PieceSupport::printSupport() const
{
	
	for (int poz = 0; poz < noPiecesOnSupport; poz++)
		std::cout << "  " << poz + kIndexOffset << "   ";
	std::cout << std::endl;
	for (int index = 0; index < noPiecesOnSupport; index++)
	{
		std::cout << "(";
		std::cout<<PiecesOnSupport[index];
		std::cout << ") ";
	}
	std::cout << std::endl;
}*/

Piece PieceSupport::getPiece(int index)
{
	return PiecesOnSupport[index- kIndexOffset];
}

std::ostream & operator<<(std::ostream & os, const PieceSupport & pieceSupport)
{
	for (int poz = 0; poz < pieceSupport.noPiecesOnSupport; poz++)
		os << "  " << poz + kIndexOffset << "   ";
	os << std::endl;
	for (int index = 0; index < pieceSupport.noPiecesOnSupport; index++)
	{
		os << "(";
		os<<pieceSupport.PiecesOnSupport[index];
		os << ") ";
	}
	os << std::endl;
	return os;
}
