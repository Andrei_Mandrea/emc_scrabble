#pragma once
class Color {
public:
	uint8_t GREEN = 32;
	uint8_t BLUE = 16;
	uint8_t WHITE = 240;
	uint8_t PURPLE = 208;
	uint8_t RED = 64;
	uint8_t BLACK = 15;
};