#include "Game.h"
#include "Board.h"
#include "Player.h"
#include "Bag.h"
#include <string>
#include "..\Logging\Logging\Logging.h"
std::ofstream of("syslog.log", std::ios::app);
Logger logger(of);
std::string myString = "";
auto dllCall = [](std::string & myString,Logger logger,Logger::Message message,Logger::Level level) {
	myString = logger.showMessage(message, level);
	logger.log(myString);
};
void printGame(Board& board, std::reference_wrapper<Player>& activePlayer)
{
	std::cout << "------------------------------------------SCRABBLE-------------------------------------------\n";
	std::cout << board;
	std::cout << "---------------------------------------------------------------------------------------------\n";
	std::cout << "You have the following Options : | (1) Mix    | (2) Change       | (3) PutOnBoard    |\n";
	std::cout << "                                 | (4) Refill | (5) CheckTheWord | (6) End your turn |\n";
	std::cout << "---------------------------------------------------------------------------------------------\n";
	std::cout << activePlayer.get();
	std::cout << "---------------------------------------------------------------------------------------------\n";
}
void Game::Run()
{

	//initialization
	Board board;
	Bag bag;
	dllCall(myString, logger, Logger::Message::Start, Logger::Level::Info);
	std::cout << "------------------------------------------SCRABBLE-------------------------------------------\n";
	std::string playerName;

	std::cout << "First player name: ";
	std::cin >> playerName;
	
	Player firstPlayer(playerName, bag);
	std::cout << "Second player name: ";
	std::cin >> playerName;
	Player secondPlayer(playerName, bag);
	int scoreToReach = 9999;
	std::cout << "Enter your score target to win: ";
	std::cin >> scoreToReach;

	std::reference_wrapper<Player> activePlayer = firstPlayer;
	std::reference_wrapper<Player> inactivePlayer = secondPlayer;
	int x = 0; //x=stop game
	while (x==0)
	{
		
		
		system("cls");
		std::cout << "------------------------------------------SCRABBLE-------------------------------------------\n";
		std::cout << board;
		std::cout << "---------------------------------------------------------------------------------------------\n";
		std::cout << "You have the following Options : | (1) Mix    | (2) Change       | (3) PutOnBoard    |\n"; 
		std::cout << "                                 | (4) Refill | (5) CheckTheWord | (6) End your turn |\n";
		std::cout << "---------------------------------------------------------------------------------------------\n";
		std::cout << activePlayer.get();
		std::cout << "---------------------------------------------------------------------------------------------\n";

		
		int choice;
		do {
			std::cout << "Please choose your option : ";
			std::cin >> choice;

			switch (choice)
			{
			case 1:
				system("cls");
				
				activePlayer.get().mixPieces();
				printGame(board,activePlayer);
				std::cout << "\n";
				break;

			case 2:
				activePlayer.get().changePieces(std::cin);
				system("cls");
				printGame(board, activePlayer);
				std::cout << "\n";
				break;
			case 3:
				
				activePlayer.get().putInCell(std::cin, board);
				dllCall(myString, logger, Logger::Message::PlayerPlaced, Logger::Level::Info);
				system("cls");
				printGame(board, activePlayer);
				std::cout << "\n";
				break;
			case 4:
				system("cls");
				activePlayer.get().refillSupport();
				printGame(board, activePlayer);
				std::cout << "\n";
				dllCall(myString, logger, Logger::Message::PlayerRefilled, Logger::Level::Info);
				break;
			case 5:
				
				activePlayer.get().checkTheWord(std::cin, board);
				system("cls");
				printGame(board, activePlayer);
				if (activePlayer.get().getPlayerScore() >= scoreToReach)
				{
					std::cout << activePlayer.get().getPlayerName() << " You Won\n";
					std::cout << "END GAME\n";
						x = 1;
						choice = 6;
				}
				std::cout << "\n";
				break;
			case 6:
				std::cout << "End your Turn \n";
				dllCall(myString, logger, Logger::Message::PlayerChanged, Logger::Level::Info);
				break;

			}

		
		} while (choice != 6);

		std::swap(activePlayer, inactivePlayer);
	
	}


}
