#pragma once
#include <ostream>
#include <vector>
#include "Bag.h"
#include "Piece.h"

class PieceSupport
{
private:
	std::vector<Piece> PiecesOnSupport;
	static const int noPiecesOnSupport=7;
	Bag bag;

public:
	PieceSupport();
	PieceSupport( Bag& m_bag);

	void mixPieces();
	void changePieces(int pieceToDrop);
	void refillSupport();
	//void printSupport() const;

	Piece getPiece(int index);
	friend std::ostream& operator << (std::ostream& os, const PieceSupport& pieceSupport);

};