#include "Board.h"
#include"Cell.h"
#include<iostream>
#include <string>
#include"Piece.h"
Board::Board() :
	firstWord(false)
{
	readWords();
	cells.resize(noOfCells);
	for (int i = 0; i < noOfCells; i++)
	{
		cells[i] = Cell(i % 15, i / 15);
		cells[i].setColor(color.GREEN);
	}
	std::vector<int>doubleLetterScore = { 3,11,36,38,45,52,59,92,96,98,102,108,116,122,126,128,132,165,172,179,186,188,213,221 };
	for (int index : doubleLetterScore)
	{
		cells[index].setBonus(Cell::DoubleLetterScore);
		cells[index].setColor(color.WHITE);
	}
	std::vector<int>tripleLetterScore = { 20,24,76,80,84,88,136,140,144,148,200,204 };
	for (int index : tripleLetterScore)
	{
		cells[index].setBonus(Cell::TripleLetterScore);
		cells[index].setColor(color.BLUE);
	}
	std::vector<int>doubleWordScore = { 16,28,32,42,48,56,64,70,112,154,160,168,176,182,192,196,208 };
	for (int index : doubleWordScore)
	{
		cells[index].setBonus(Cell::DoubleWordScore);
		cells[index].setColor(color.PURPLE);
	}
	std::vector<int>tripleWordScore = { 0,7,14,105,119,210,217,224 };
	for (int index : tripleWordScore)
	{
		cells[index].setBonus(Cell::TripleWordScore);
		cells[index].setColor(color.RED);
	}
}

bool Board::putInCell(int m_x, int m_y,const Piece& p)
{
	m_x -= 1;
	m_y -= 1;
	if (cells[m_x * 15 + m_y].getPiece().getCharacter() == '!')
	{
		cells[m_x * 15 + m_y].setPiece(p);
		//p.setCell(cells[m_y * 15 + m_x]);
		return true;
	}
	else
		if (m_x < 0 || m_x>14 || m_y < 0 || m_y>14)
		{
			std::cout << "Out of bounds";
			return false;
		}

	std::cout << "There is another piece on that cell, choose another position \n";
	return false;
}

bool Board::canPutCell(const std::string& word , int& index, const int& direction)
{
	bool crossing = false;
	bool center = false;
	bool viableSize = false;
	int startingIndex = index;
	unsigned int added = 0;
	if ((direction == 0 && index % 15 != 0 && cells[index - 1].getPiece().getCharacter() != '!') || (index / 15 != 0 && cells[index - 15].getPiece().getCharacter() != '!'))
		crossing = true;
	while (1)
	{
		if ((direction == 0 && index / 15 != startingIndex / 15) || index > 224 || added == word.size())
			break;
		if (index == 112)
			center = true;
		if (cells[index].getPiece().getCharacter() != '!')
			crossing = true;
		else
		{
			if (direction == 0 && ((index / 15 != 0 && cells[index - 15].getPiece().getCharacter() != '!') || (index / 15 != 14 && cells[index + 15].getPiece().getCharacter() != '!')))
				crossing = true;
			if ((index % 15 != 0 && cells[index - 1].getPiece().getCharacter() != '!') || (index % 15 != 14 && cells[index + 1].getPiece().getCharacter() != '!'))
				crossing = true;
			added++;
		}

		if (direction == 0)
			index++;
		else
			index += 15;
	}
	if (index >= 0 && index <= 224 && cells[index].getPiece().getCharacter() != '!')
		crossing = true;
	viableSize = (added == word.size());
	if (!firstWord && center && viableSize)
	{
		firstWord = true;
		return true;
	}
	return viableSize && crossing;
}

Piece Board::getPiece(const int& index) const
{
	return cells[index].getPiece();
}

Cell::Bonus Board::getBonus(const int& index) const
{
	return cells[index].getBonus();
}

void Board::readWords()
{
	std::ifstream input("words.txt");
	std::string word;
	while (std::getline(input, word))
	{
		words.push_back(word);
	}
}

bool Board::checkWord(std::string& wordFromPlayer) const
{
	for (std::string wordFromBoard : words)
	{
		if (wordFromBoard == wordFromPlayer)
			return true;
	}
	return false;
}

bool Board::checkWordOnBoard(int x,int y, std::string& word,std::string& direction)
{
	x -= 1;
	y -= 1;
	if (checkWord(word) == false)
	{
		std::cout << "The word : " << word << " does not exist\n";
		return false;
	}
	if (direction == "right")
	{
		for (int index = 0; index < word.size(); index++)
			if (cells[x * 15 + index + y].getPiece().getCharacter() != word[index])
			{
				std::cout << "The word : " << word << " it's not on table\n";
				return false;
			}
		return true;
	}
	if (direction == "down")
	{
		for (int index = 0; index < word.size(); index++)
			if (cells[(index + x) * 15 + y].getPiece().getCharacter() != word[index])
			{
				std::cout << "The word : " << word << " it's not on table\n";
				return false;
			}
		return true;
	}
}

int Board::scoreForWord(int x, int y, std::string & word, std::string & direction)
{

	int scoreForWord = 0;
	if (checkWordOnBoard(x, y, word, direction) == true)
	{
		x -= 1;
		y -= 1;
		if (direction == "right")
		{
			int multiply = 1;
			for (int index = 0; index < word.size(); index++)
			{
				scoreForWord += cells[x * 15 + index + y].getScoreByBonus();
				if (cells[x * 15 + index + y].getBonus() == 2)
					multiply = 2;
				if (cells[x * 15 + index + y].getBonus() == 3)
					multiply = 3;
			}
			scoreForWord *= multiply;
		}
		if (direction == "down")
		{
			int multiply = 1;
			for (int index = 0; index < word.size(); index++)
			{
				scoreForWord += cells[(index + x) * 15 + y].getScoreByBonus();
				if (cells[(index + x) * 15 + y].getBonus() == 2)
					multiply = 2;
				if (cells[(index + x) * 15 + y].getBonus() == 3)
					multiply = 3;
			}
			scoreForWord *= multiply;
		}
	}
	return scoreForWord;
}



void Board::printBoard() const
{
	std::cout << std::endl;
	//std::cout << "  ";
	//Printing first line of table row's
	std::cout << "    ";
	for (int index = 0; index < boardSize; index++)
	{
		if (index <= 8)
			std::cout << index + 1 << " |";
		else
			std::cout << index + 1 << "|";

	}
	std::cout << std::endl;

	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	int indexCells = 0;
	for (int row = 0; row < boardSize; row++)
	{
		SetConsoleTextAttribute(hConsole, color.BLACK);
		if (row <= 8)
			std::cout << row + 1 << "  |";
		if (row >= 9)
			std::cout << row + 1 << " |";
		int contorSpace = 0;

		for (int col = row; col < boardSize + row; col++)
		{
			//if(cells[indexCells].getColor()==color.GREEN)
			SetConsoleTextAttribute(hConsole, cells[indexCells].getColor());

			//if (contorSpace < 9)

			std::cout << "_" << cells[indexCells].getPiece().getCharacter() << "|";

			/*else
			std::cout << "__" << cells[indexCells].getPiece().getCharacter() << "|";
			*/
			contorSpace++;
			indexCells++;
			SetConsoleTextAttribute(hConsole, color.BLACK);
		}

		std::cout << std::endl;
	}

	SetConsoleTextAttribute(hConsole, color.BLACK);
	std::cout << std::endl;


}



std::ostream & operator<<(std::ostream & os, const Board & board)
{
	os << std::endl;
	//std::cout << "  ";
	//Printing first line of table row's
	os << "    ";
	for (int index = 0; index < board.boardSize; index++)
	{
		if (index <= 8)
			os << index + 1 << " |";
		else
			os << index + 1 << "|";

	}
	os << std::endl;

	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	int indexCells = 0;
	for (int row = 0; row <board.boardSize; row++)
	{
		SetConsoleTextAttribute(hConsole, board.color.BLACK);
		if (row <= 8)
			os<< row + 1 << "  |";
		if (row >= 9)
			os << row + 1 << " |";
		int contorSpace = 0;

		for (int col = row; col < board.boardSize + row; col++)
		{
			//if(cells[indexCells].getColor()==color.GREEN)
			SetConsoleTextAttribute(hConsole,board.cells[indexCells].getColor());

			//if (contorSpace < 9)

			os << "_" << board.cells[indexCells].getPiece().getCharacter() << "|";

			/*else
			std::cout << "__" << cells[indexCells].getPiece().getCharacter() << "|";
			*/
			contorSpace++;
			indexCells++;
			SetConsoleTextAttribute(hConsole, board.color.BLACK);
		}

		os << std::endl;
	}

	SetConsoleTextAttribute(hConsole, board.color.BLACK);
	os << std::endl;

	return os;
}
