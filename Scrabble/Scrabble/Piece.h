#pragma once
//#include "Cell.h"
#include <stdint.h>
#include<ostream>
class Piece
{
private:
	char character;
	int point;
public:
	Piece(const char& m_character='!', const int& m_point=-1);
	char getCharacter() const;
	int getPoints() const;
	void setPieceAsDefault();
	bool compare(const Piece& piece2) const;
	friend std::ostream& operator<<(std::ostream& os, const Piece& p);
};

