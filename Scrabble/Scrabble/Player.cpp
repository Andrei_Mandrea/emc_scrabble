#include "Player.h"


Player::Player(const std::string & newName, Bag & bag) : 
	playerName(newName),
	playerScore(0),
	pieceSupport(bag)
{
	
}

std::string Player::getPlayerName() const
{
	return playerName;
}
int Player::getPlayerScore() const
{
	return playerScore;
}

void Player::setPlayerScore( const int& score)
{
	this->playerScore = score;
}

PieceSupport Player::getPieceSupport() const
{
	return pieceSupport;
}

void Player::changePieces(std::istream& in)
{
	int noToChange;
	std::cout << "Insert number of Pieces to Change: ";
	in >> noToChange;
	std::cout << "Insert indexes of pieces to Change: ";
	for (int index = 0; index < noToChange; index++)
	{
		int pieceToChange;
		in >> pieceToChange;
		pieceSupport.changePieces(pieceToChange);
	}
	pieceSupport.refillSupport();
}

void Player::refillSupport()
{
	pieceSupport.refillSupport();
}

void Player::mixPieces()
{
	pieceSupport.mixPieces();
}


void Player::checkTheWord(std::istream& in, Board& board)
{
	std::cout << "Enter the word found: ";
	std::string word;
	in >> word;
	int x, y;
	std::cout << "Enter the position of first letter of your word: ";
	in >> x >> y;
	std::string direction;
	std::cout << "Enter direction [(right)=word start from left to right]/[(down)= word start from up to down]: ";
	in >> direction;
	if (board.checkWordOnBoard(x, y, word, direction) == true)
		playerScore += board.scoreForWord(x, y, word, direction);

}

void Player::putInCell(std::istream & in,Board& board)
{
	while (true) {
		std::cout << "Select Piece to put on Board, insert index: ";
		int index;
		in >> index;
		if (pieceSupport.getPiece(index).getCharacter() != '!')
		{
			int x, y;
			std::cout << "Insert position(X,Y): ";
			in >> x >> y;
			board.putInCell(x, y, pieceSupport.getPiece(index));
			pieceSupport.changePieces(index);
			break;
		}
		else std::cout << "This is an empty piece, choose another one\n";
	}
}

void Player::printPlayer() const
{
	std::cout << "|Name|: " << playerName << "|Score|: " << playerScore << std::endl;
}

std::ostream & operator<<(std::ostream & os, const Player & player)
{
	os << "|Name| : " << player.playerName << std::endl << "|Score|: " << player.playerScore << std::endl;
	os<<player.pieceSupport;

	return os;
}
