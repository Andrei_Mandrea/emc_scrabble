#pragma once
#include"Piece.h"
#include <stdint.h>
#include "Color.h"
class Cell
{
public:
	enum Bonus {
		DoubleLetterScore,  // =0 white
		TripleLetterScore, //  =1 blue
		DoubleWordScore,  //   =2 purple
		TripleWordScore, //    =3 red
		Simple			//     =4 simple
	};
private:
	int x;
	int y;
	Piece piece;
	Bonus bonus;
	int color;
public:
	Cell(const int& m_x=-1 ,const int& m_y=-1);
	Piece getPiece() const;
	void setPiece(const Piece& p);
	Bonus getBonus() const;
	void setBonus(const Bonus& b);
	int getColor() const;
	void setColor(const int& color);

	int getScoreByBonus();

};
