#pragma once
#include <algorithm>
#include <iterator>
#include"Cell.h"
#include<vector>
#include <fstream>
#include <Windows.h>
#include "Color.h"

class Board
{
private:
	static const int noOfCells = 225;
	static const int boardSize = 15;
	std::vector<Cell>cells;
	bool firstWord;
	Color color;
	std::vector<std::string> words;
	
public:
	Board();
	bool putInCell(int m_x, int m_y, const Piece& p);
	bool canPutCell(const std::string& word, int& index, const int& direction);
	Piece getPiece(const int& index)const;
	Cell::Bonus getBonus(const int& index) const;

	void readWords();
	bool checkWord(std::string& wordFromPlayer) const;
	bool checkWordOnBoard(int x, int y, std::string& word,std::string& direction);
	int scoreForWord(int x, int y, std::string& word, std::string& direction);

	void printBoard() const;
	friend std::ostream& operator << (std::ostream& os, const Board& board);


};