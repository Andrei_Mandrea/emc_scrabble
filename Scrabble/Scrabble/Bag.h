#pragma once
#include<vector>
#include "Piece.h"

class Bag
{
private:
	int noOfPieces;
	static const int initialNoOfPieces = 100;
	std::vector<Piece> pieces;

public:
	Bag();
	int getNoOfPieces() const;
	void addOnePiece(const Piece& piece);
	void removeOnePiece(Piece& piece);
	void printVectorOfPieces(std::ostream& os) const;
	Piece getPiece() ;
};

